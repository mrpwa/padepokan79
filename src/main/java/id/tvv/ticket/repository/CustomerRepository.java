package id.tvv.ticket.repository;

import id.tvv.ticket.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Page<Customer> findAll(Pageable pageable);

    Page<Customer> findAll(Specification<Customer> specification, Pageable pageable);

    List<Customer> findAll(Specification<Customer> specification);

    Customer findFirstByNameEqualsIgnoreCase(String name);

    Customer findFirstByNameContainingIgnoreCase(String name);
}
