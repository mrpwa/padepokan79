package id.tvv.ticket.repository;

import id.tvv.ticket.model.Customer;
import id.tvv.ticket.model.TransactionActivity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionActivityRepository extends JpaRepository<TransactionActivity, Long> {

    Page<TransactionActivity> findAll(Pageable pageable);

    Page<TransactionActivity> findAll(Specification<TransactionActivity> specification, Pageable pageable);

    List<TransactionActivity> findAll(Specification<TransactionActivity> specification);

    List<TransactionActivity> findAllByCustomerAndIsDeletedAndDebitCreditStatsEquals(Customer cus, boolean isDeleted, String debitCreditStat);

//    TransactionActivity findFirstByNameEqualsIgnoreCase(String name);
}
