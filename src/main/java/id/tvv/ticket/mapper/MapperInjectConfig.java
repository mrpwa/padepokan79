package id.tvv.ticket.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.MapperConfig;

@MapperConfig(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = InstantStringMapper.class)
public class MapperInjectConfig {
}
