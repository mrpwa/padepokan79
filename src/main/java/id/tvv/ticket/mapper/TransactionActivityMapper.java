package id.tvv.ticket.mapper;

import id.tvv.ticket.dto.TransactionActivityDto;
import id.tvv.ticket.dto.TransactionReportDto;
import id.tvv.ticket.model.TransactionActivity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;

@Component
@Mapper(config = MapperInjectConfig.class, uses = {CustomerMapper.class})
public interface TransactionActivityMapper {

    @Mappings({
            @Mapping(target = "customerId", source = "customer.id"),
            @Mapping(target = "customerName", source = "customer.name"),
    })
    public TransactionActivityDto toDto(TransactionActivity entity);

    @Mappings({
            @Mapping(target = "credit", expression = "java( entity.getDebitCreditStats().equals(\"C\") ? entity.getAmount().toString() : \"-\" )"),
            @Mapping(target = "debit", expression = "java( entity.getDebitCreditStats().equals(\"D\") ? entity.getAmount().toString() : \"-\" )"),
    })
    public TransactionReportDto toReportDto(TransactionActivity entity);

    public TransactionActivity toEntity(TransactionActivityDto dto);

}
