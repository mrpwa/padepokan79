package id.tvv.ticket.mapper;

import id.tvv.ticket.dto.CustomerDto;
import id.tvv.ticket.model.Customer;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(config = MapperInjectConfig.class)
public interface CustomerMapper {

    public CustomerDto toDto(Customer entity);

    public Customer toEntity(CustomerDto dto);

}
