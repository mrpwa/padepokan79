package id.tvv.ticket.mapper;

import org.mapstruct.Mapper;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@Mapper(componentModel = "spring")
public class InstantStringMapper {

    public Instant fromStringtoInstant(String instant) {
        if (instant == null) {
            return null;
        }
        final DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss")
                .withZone(ZoneId.of("UTC"));
        return Instant.from(formatter.parse(instant));
    }

    public String fromInstanttoString(Instant instant) {
        if (instant == null) {
            return null;
        }
        final DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss")
                .withZone(ZoneId.of("UTC"));
        return formatter.format(instant);
    }
}
