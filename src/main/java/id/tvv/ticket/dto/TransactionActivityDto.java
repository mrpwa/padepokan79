package id.tvv.ticket.dto;


import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import id.tvv.ticket.model.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class TransactionActivityDto implements Serializable {

    private Long id;
    private LocalDateTime transactionDate;
    private String description;
    private String debitCreditStats;
    private Integer amount;
    private Long customerId;
    private String customerName;

    //search
    private String start;
    private String end;
}
