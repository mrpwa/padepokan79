package id.tvv.ticket.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatasourceBuilderConfiguration {

    @Bean
    @ConfigurationProperties("spring.datasource.hikari")
    public HikariDataSource dataSource(DataSourceProperties properties) {
        HikariDataSource ds = properties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
        ds.setIdleTimeout(10000);
        Integer availProc = Runtime.getRuntime().availableProcessors();
        if (availProc < 8) {
            ds.setMinimumIdle(availProc);
            ds.setMaximumPoolSize(availProc * 2);
        } else {
            ds.setMaximumPoolSize(availProc);
        }
        return ds;
    }
}
