package id.tvv.ticket.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.Optional;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class JpaAuditConfiguration {

	@Bean
	public AuditorAware<String> auditorProvider() {
		return () -> {
			if (SecurityContextHolder.getContext().getAuthentication() != null) {
				try {
					Jwt principal = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
					return Optional.ofNullable(principal.getClaimAsString("userName"));
				} catch (Exception e) {
					return Optional.ofNullable("Unknown");
				}
//				return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
			} else {
				return Optional.ofNullable("Unknown");
			}
		};
	}
}
