package id.tvv.ticket.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="padepokan_transaction_history")
@EqualsAndHashCode(callSuper = false)
public class TransactionActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private Long id;
    @Column(name = "transaction_date", nullable = false)
    private LocalDateTime transactionDate;
    private String description;
    @Column(name = "debit_credit_stats", nullable = false, length = 1)
    private String debitCreditStats;
    @Column(name = "amount", nullable = false)
    private Integer amount;
    @Column(name = "is_deleted", nullable = false, columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean isDeleted;

    @ToString.Exclude
    @JsonIgnore
    @JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = false)
    @ManyToOne
    private Customer customer;
}
