package id.tvv.ticket.util;

public class PointUtil {

    public static double getPoint(String description, int qty) {
        if (description.toLowerCase().contains("pulsa")) {
            return PointUtil.pointPulsa(qty);
        } else if (description.toLowerCase().contains("listrik")) {
            return PointUtil.pointListrik(qty);
        }

        return 0;
    }

    public static double pointPulsa(int qty) {
        int total = 0;
        int counter = 1;
        double point = 0;
        int temp = qty;
        while(total < qty) {
            boolean isgreater = temp > 30000;
            int raw = isgreater ? 30000 : temp;
            if (counter == 1) {
                point = 0;
                total = isgreater ? total + 30000 : total + raw;
            } else if (counter == 2) {
                point = point + ((double) raw/1000.0);
                total = isgreater ? total + 30000 : total + raw;
            } else {
                point = point + ((double) raw/1000.0 * 2);
                total = isgreater ? total + 30000 : total + raw;
            }

            counter++;
            temp = qty - total;
        }

        return point;
    }

    public static double pointListrik(int qty) {
        int total = 0;
        int counter = 1;
        double point = 0;
        int temp = qty;
        while(total < qty) {
            boolean isgreater = temp > 50000;
            int raw = isgreater ? 50000 : temp;
            if (counter == 1) {
                point = 0;
                total = isgreater ? total + 50000 : total + raw;
            } else if (counter == 2) {
                point = point + ((double)raw / 2000.0);
                total = isgreater ? total + 50000 : total + raw;
            } else {
                point = point + ((double)raw / 2000.0 * 2);
                total = isgreater ? total + 50000 : total + raw;
            }

            counter++;
            temp = qty - total;
        }

        return point;
    }
}
