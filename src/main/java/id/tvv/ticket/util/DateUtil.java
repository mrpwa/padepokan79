package id.tvv.ticket.util;

import com.google.common.base.Strings;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateUtil {

    public static LocalDateTime toUTCLocalDateTime(String str) {
        if (Strings.isNullOrEmpty(str)) return null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("UTC"));
        return LocalDateTime.parse(str, formatter);
    }
}
