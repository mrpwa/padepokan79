package id.tvv.ticket.service;

import com.google.common.base.Strings;
import id.tvv.ticket.dto.CustomerDto;
import id.tvv.ticket.mapper.CustomerMapper;
import id.tvv.ticket.model.Customer;
import id.tvv.ticket.model.TransactionActivity;
import id.tvv.ticket.repository.CustomerRepository;
import id.tvv.ticket.repository.TransactionActivityRepository;
import id.tvv.ticket.specification.CustomerSpecification;
import id.tvv.ticket.util.PointUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional(readOnly = true)
public class CustomerService {

    @Autowired
    private CustomerRepository repository;
    @Autowired
    private TransactionActivityRepository transactionActivityRepository;
    @Autowired
    private CustomerMapper mapper;

    public List<CustomerDto> getAll(CustomerDto filter) {
        Specification<Customer> specification = CustomerSpecification.byEntitySearch(filter);
        return StreamSupport.stream(repository.findAll(specification).spliterator(), false)
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    public Page<CustomerDto> getAll(CustomerDto filter, Pageable pageable) {
        Specification<Customer> specification = CustomerSpecification.byEntitySearch(filter);
        return repository.findAll(specification, pageable)
                .map(mapper::toDto);
    }

    public CustomerDto getById(Long id) {
        Customer customer = repository.findById(id).orElseThrow();
        return mapper.toDto(customer);
    }

    public List<CustomerDto> getAllPoints() {
        List<CustomerDto> dtos = new ArrayList<>();
        List<Customer> customers = repository.findAll();
        customers.forEach(cus -> {
            List<TransactionActivity> trans = transactionActivityRepository.findAllByCustomerAndIsDeletedAndDebitCreditStatsEquals(cus, false, "D");
            List<Double> points = new ArrayList<>();
            trans.forEach(tran -> {
                double point = PointUtil.getPoint(tran.getDescription(), tran.getAmount());
                points.add(point);
            });
            CustomerDto dto = mapper.toDto(cus);
            Double finalPoint = points.stream()
                    .reduce(0D , (num1, num2) -> num1 + num2);
            dto.setPoint(finalPoint.intValue());
            dtos.add(dto);
        });
        return dtos;
    }

    @Transactional(readOnly = false)
    public CustomerDto update(Long id, CustomerDto input) {
        Customer old = repository.findById(id).orElseThrow();
        Customer updater = mapper.toEntity(input);
        Customer updated = this.mapper(old, updater);
        updated = repository.save(updated);
        return mapper.toDto(updated);
    }

    @Transactional(readOnly = false)
    public CustomerDto create(CustomerDto input) {
        Customer customer = mapper.toEntity(input);
        customer = repository.save(customer);
        return mapper.toDto(customer);
    }

    @Transactional(readOnly = false)
    public String delete(Long id) {
        Customer cat = repository.findById(id).orElseThrow();
        repository.deleteById(id);
        return " Account "  + cat.getName() + " telah dihapus.";
    }

    private Customer mapper(Customer old, Customer updated) {
        Customer ret = old;
        if (updated.getName() != null && !updated.getName().isBlank()) {
            ret.setName(updated.getName());
        }

        return ret;
    }
}
