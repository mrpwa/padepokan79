package id.tvv.ticket.service;

import id.tvv.ticket.dto.TransactionActivityDto;
import id.tvv.ticket.dto.TransactionReportDto;
import id.tvv.ticket.mapper.TransactionActivityMapper;
import id.tvv.ticket.model.Customer;
import id.tvv.ticket.model.TransactionActivity;
import id.tvv.ticket.repository.CustomerRepository;
import id.tvv.ticket.repository.TransactionActivityRepository;
import id.tvv.ticket.specification.TransactionActivitySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional(readOnly = true)
public class TransactionActivityService {

    @Autowired
    private TransactionActivityRepository repository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private TransactionActivityMapper mapper;

    public List<TransactionActivityDto> getAll(TransactionActivityDto filter) {
        Specification<TransactionActivity> specification = TransactionActivitySpecification.byEntitySearch(filter);
        return StreamSupport.stream(repository.findAll(specification).spliterator(), false)
                .map(mapper::toDto)
                .sorted(Comparator.comparing(TransactionActivityDto::getTransactionDate))
                .collect(Collectors.toList());
    }

    public Page<TransactionActivityDto> getAll(TransactionActivityDto filter, Pageable pageable) {
        Specification<TransactionActivity> specification = TransactionActivitySpecification.byEntitySearch(filter);
        return repository.findAll(specification, pageable)
                .map(mapper::toDto);
    }

    public List<TransactionReportDto> getReport(TransactionActivityDto filter) {
        Specification<TransactionActivity> specification = TransactionActivitySpecification.byEntitySearch(filter);
        return StreamSupport.stream(repository.findAll(specification).spliterator(), false)
                .map(mapper::toReportDto)
                .sorted(Comparator.comparing(TransactionReportDto::getTransactionDate))
                .collect(Collectors.toList());
    }

    public TransactionActivityDto getById(Long id) {
        TransactionActivity activity = repository.findById(id).orElseThrow();
        return mapper.toDto(activity);
    }

//    @Transactional(readOnly = false)
//    public TransactionActivityDto update(Long id, TransactionActivityDto input) {
//        TransactionActivity old = repository.findById(id).orElseThrow();
//        TransactionActivity updater = mapper.toEntity(input);
//        TransactionActivity updated = this.mapper(old, updater);
//        updated = repository.save(updated);
//        return mapper.toDto(updated);
//    }

    @Transactional(readOnly = false)
    public TransactionActivityDto create(TransactionActivityDto input) {
        TransactionActivity activity = mapper.toEntity(input);
        Customer cus = customerRepository.findById(input.getCustomerId()).orElse(null);
        activity.setCustomer(cus);
        activity.setDebitCreditStats("D");
        if (input.getDescription().equalsIgnoreCase("setor tunai")) activity.setDebitCreditStats("C");
        activity.setTransactionDate(LocalDateTime.now());
        activity.setIsDeleted(false);
        activity = repository.save(activity);
        return mapper.toDto(activity);
    }

    @Transactional(readOnly = false)
    public String delete(Long id) {
        TransactionActivity cat = repository.findById(id).orElseThrow();
        cat.setIsDeleted(true);
        repository.save(cat);
        return "Data telah dihapus.";
    }

    private TransactionActivity mapper(TransactionActivity old, TransactionActivity updated) {
        TransactionActivity ret = old;
        if (updated.getDescription() != null && !updated.getDescription().isBlank()) {
            ret.setDescription(updated.getDescription());
        }

        return ret;
    }
}
