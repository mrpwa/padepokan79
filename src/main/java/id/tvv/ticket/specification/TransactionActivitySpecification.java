package id.tvv.ticket.specification;

import id.tvv.ticket.dto.TransactionActivityDto;
import id.tvv.ticket.model.TransactionActivity;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class TransactionActivitySpecification {

    private static Specification<TransactionActivity> attributeContains(String attribute, String value) {
        if (value != null && !value.isBlank()) {
            final String finalValue = "%" + value.toLowerCase() + "%";
            return (root, query, cb) -> {
                return cb.like(
                        cb.lower(root.get(attribute)),
                        finalValue
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<TransactionActivity> attributeNotEqual(String attribute, String value) {
        if (value != null && !value.isBlank()) {
            final String finalText = value.toLowerCase();
            return (root, query, cb) -> {
                return cb.notEqual(
                        cb.lower(root.get(attribute)),
                        finalText
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<TransactionActivity> attributeEqual(String attribute, Object value) {
        if (value != null) {
            final Object finalText = value;
            return (root, query, cb) -> {
                return cb.equal(
                        root.get(attribute),
                        finalText
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<TransactionActivity> relationEqual(String attribute, Object value) {
        if (value != null) {
            final Object finalText = value;
            String[] attrParts = attribute.split("\\.");
            return (root, query, cb) -> {
                return cb.equal(
                        root.get(attrParts[0]).get(attrParts[1]),
                        finalText
                );
            };

        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<TransactionActivity> attributeGtDate(String attribute, String value) {
        if (value == null || value.isBlank()) {
            return null;
        }
        ZoneId zoneId = ZoneId.of("Asia/Jakarta");
        LocalDateTime ndate = LocalDate.parse(value, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                .atStartOfDay(zoneId)
                .withZoneSameInstant(ZoneId.of("+07:00"))
                .toLocalDateTime();
        return (root, query, cb) -> {

            return cb.greaterThanOrEqualTo(
                    root.get(attribute).as(LocalDateTime.class),
                    ndate
            );
        };
    }

    private static Specification<TransactionActivity> attributeLtDate(String attribute, String value) {
        if (value == null || value.isBlank()) {
            return null;
        }
        ZoneId zoneId = ZoneId.of("Asia/Jakarta");
        LocalDateTime ndate = LocalDate.parse(value, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                .plusDays(1)
                .atStartOfDay(zoneId)
                .withZoneSameInstant(ZoneId.of("+07:00"))
                .toLocalDateTime()
                .minusNanos(1);
        return (root, query, cb) -> {

            return cb.lessThanOrEqualTo(
                    root.get(attribute).as(LocalDateTime.class),
                    ndate
            );
        };
    }


    private static Specification<TransactionActivity> byIsdeleted(Boolean name) {
        return attributeEqual("isDeleted", name);
    }

    private static Specification<TransactionActivity> byCustomer(Long arg) {
        return relationEqual("customer.id", arg);
    }

    private static Specification<TransactionActivity> byGtDate(String arg) {
        return attributeGtDate("transactionDate", arg);
    }

    private static Specification<TransactionActivity> byLtDate(String arg) {
        return attributeLtDate("transactionDate", arg);
    }

    public static Specification<TransactionActivity> byEntitySearch(TransactionActivityDto filter) {
        if (filter == null) {
            return null;
        }
        return byIsdeleted(false)
                .and(byCustomer(filter.getCustomerId()))
                .and(byGtDate(filter.getStart()))
                .and(byLtDate(filter.getEnd()))
            ;
    }
}
