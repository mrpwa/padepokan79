package id.tvv.ticket.specification;

import id.tvv.ticket.dto.CustomerDto;
import id.tvv.ticket.model.Customer;
import org.springframework.data.jpa.domain.Specification;

public class CustomerSpecification {

    private static Specification<Customer> attributeContains(String attribute, String value) {
        if (value != null && !value.isBlank()) {
            final String finalValue = "%" + value.toLowerCase() + "%";
            return (root, query, cb) -> {
                return cb.like(
                        cb.lower(root.get(attribute)),
                        finalValue
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<Customer> attributeNotEqual(String attribute, String value) {
        if (value != null && !value.isBlank()) {
            final String finalText = value.toLowerCase();
            return (root, query, cb) -> {
                return cb.notEqual(
                        cb.lower(root.get(attribute)),
                        finalText
                );
            };
        }
        return (root, query, cb) -> {
            return null;
        };
    }

    private static Specification<Customer> byName(String name) {
        return attributeContains("name", name);
    }

    public static Specification<Customer> byEntitySearch(CustomerDto filter) {
        if (filter == null) {
            return null;
        }
        return byName(filter.getName())
            ;
    }
}
