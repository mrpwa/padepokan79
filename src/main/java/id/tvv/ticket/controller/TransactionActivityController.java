package id.tvv.ticket.controller;

import id.tvv.ticket.dto.TransactionActivityDto;
import id.tvv.ticket.dto.TransactionReportDto;
import id.tvv.ticket.service.TransactionActivityService;
import id.tvv.ticket.util.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/transaction-activity")
public class TransactionActivityController {

    private final TransactionActivityService service;

    public TransactionActivityController(TransactionActivityService service) {
        this.service = service;
    }

    @GetMapping()
    public ResponseEntity<List<TransactionActivityDto>> list(TransactionActivityDto filter,
                                                             @PageableDefault(direction = Sort.Direction.ASC) final Pageable pageable) {
        try {
            Page<TransactionActivityDto> special = service.getAll(filter, pageable);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<TransactionActivityDto>> list(TransactionActivityDto filter) {
        try {
            List<TransactionActivityDto> special = service.getAll(filter);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/report")
    public ResponseEntity<List<TransactionReportDto>> report(TransactionActivityDto filter) {
        try {
            List<TransactionReportDto> special = service.getReport(filter);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        try {
            TransactionActivityDto special = service.getById(id);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable(required = true) Long id, @RequestBody TransactionActivityDto input) {
        try {
            TransactionActivityDto result = service.getById(id);
            if (result == null) {
                return ResponseEntity.notFound().build();
            }
            TransactionActivityDto special = result; //service.update(id, input);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }

    }

    @PostMapping
    public ResponseEntity<?> post(@RequestBody TransactionActivityDto input) {
        try {
            TransactionActivityDto special = service.create(input);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable(required = true) Long id) {
        try {
            String msg = service.delete(id);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", msg), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }
}
