package id.tvv.ticket.controller;

import id.tvv.ticket.dto.CustomerDto;
import id.tvv.ticket.service.CustomerService;
import id.tvv.ticket.util.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    private final CustomerService service;

    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @GetMapping()
    public ResponseEntity<List<CustomerDto>> list(CustomerDto filter,
                                                  @PageableDefault(sort = {"name"}, direction = Sort.Direction.ASC) final Pageable pageable) {
        try {
            Page<CustomerDto> special = service.getAll(filter, pageable);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<CustomerDto>> list(CustomerDto filter) {
        try {
            List<CustomerDto> special = service.getAll(filter);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        try {
            CustomerDto special = service.getById(id);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable(required = true) Long id, @RequestBody CustomerDto input) {
        try {
            CustomerDto result = service.getById(id);
            if (result == null) {
                return ResponseEntity.notFound().build();
            }
            CustomerDto special = service.update(id, input);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }

    }

    @PostMapping
    public ResponseEntity<?> post(@RequestBody CustomerDto input) {
        try {
            CustomerDto special = service.create(input);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable(required = true) Long id) {
        try {
            String msg = service.delete(id);
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", msg), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }

    @GetMapping("/points")
    public ResponseEntity<?> getAllPoint() {
        try {
            List<CustomerDto> special = service.getAllPoints();
            return new ResponseEntity(new ApiResponse<>("200", null, "Success", special), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), e);
        }
    }
}
